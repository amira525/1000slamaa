/*global $, window, document, WOW */
$(document).ready(function () {
    'use strict';
    // menu-link
    $('.collapse .navbar-nav a:first-child').click(function () {
        $('.menu-link.first').toggleClass('active');
        $('.menu-link.second').removeClass('active');
    });
    $('.collapse .navbar-nav a:last-child').click(function () {
        $('.menu-link.second').toggleClass('active');
        $('.menu-link.first').removeClass('active');
    });
    $(window).click(function () {
        $(".menu-link").removeClass("active");
    });
    
    $('.navbar-nav,.menu-link').click(function (event) {
        event.stopPropagation();
    });
    
    // fixed-menu
    $('.fixed-menu .fa-reply-all').click(function () {
        $(".fixed-menu").toggleClass("active");
    });
    // 
    new WOW().init();
    //
    $('.new-subject .subject .fa-plus').on('click', function () {
        $('.new-subject .subject .fa-plus').hide();
        $('.new-subject .subject p.on , .new-subject .subject .fa-long-arrow-alt-left').fadeIn();
    });
    // Trigger Nice Scroll
    
    $('html').niceScroll({
        
        cursorcolor: '#0a71bf',
        
        cursorwidth: '10px',
        
        cursorborder: '1px solid #0a71bf',
        
        cursorborderradius: '5px',
        
        autohidemode: 'false'
        
    });
    
    //
    $('.news ul li a').click(function () {

        $('html, body').animate({

            scrollTop: $('#' + $(this).data('scroll')).offset().top

        }, 1000);

    });
    
    //
    $('.new-link li a').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        $("html, body").animate({
            scrollTop: $('.two').offset().top - 60
        }, 500);
    });
    
    var allQuizs = 0;
    $('.one').each(function () {
        allQuizs += 1;
       
    });
    
//    $('.one').hide();
    var quizData = $('.one').attr('data-quiz'),
        numQuizData = parseInt(quizData);
    
    if (numQuizData == 1) {
        $(".one[data-quiz='1']").show();
    }
    
    $('.butt .next').click(function () {
       
        
        if (numQuizData == allQuizs) {
           
            $(".one[data-quiz='" + numQuizData + "']").show();
            $("html, body").animate({
                scrollTop:  $(".one[data-quiz='" + numQuizData + "']").offset().top - 60
            }, 500);
        } else {
            numQuizData += 1;
            $('.one').hide();
            $(".one[data-quiz='" + numQuizData + "']").show();
            $("html, body").animate({
                scrollTop:  $(".one[data-quiz='" + numQuizData + "']").offset().top - 60
            }, 500);
        }
    });
    $('.butt .back').click(function () {
        
        
        if (numQuizData == 1) {
           
            $(".one[data-quiz='" + numQuizData + "']").show();
            $("html, body").animate({
                scrollTop:  $(".one[data-quiz='" + numQuizData + "']").offset().top - 60
            }, 500);
        } else {
            numQuizData -= 1;
            $('.one').hide();
            $(".one[data-quiz='" + numQuizData + "']").show();
            $("html, body").animate({
                scrollTop:  $(".one[data-quiz='" + numQuizData + "']").offset().top - 60
            }, 500);
        }
    });
    $('.setting-pation .menu-list ul li a').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#' + $(this).data('scroll')).offset().top + 1
        }, 2000);
    });
    
    $('.Doctor-Setting .menu-list ul li a').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#' + $(this).data('scroll')).offset().top + 1
        }, 2000);
    });
    
    
    $('.tabs-list li').on('click', function () {
        $(this).addClass('select').siblings().removeClass('select');
        $('.content-list > div').hide(100);
        $($(this).data('contact')).fadeIn(200);
    });
    
    $('.contact-list .contact-one button.show').on('click', function () {
        $(this).addClass('select').siblings().removeClass('select');
        $('.form-list > div').hide();
        $($(this).data('form')).fadeIn();
    });
    $('.card-body .body .delet').on('click', function () {
        $('.card-body.finish').hide();
        $('.card-body.chack').fadeIn();
    });
    $('.card-body .butt .defl').on('click', function () {
        $('.card-body.chack').hide();
        $('.card-body.finish').fadeIn();
    });
    
    
    
    
    $('.doctor-search .part .show button').on('click', function () {
        $('.doctor-search .part .show').hide();
        $('.doctor-search .part .notes').fadeIn();
    });
    $('.doctor-search .recently .part .all .notes .day span').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        $('.doctor-search .recently .part .all .notes .day > p').addClass('select').siblings().removeClass('select');
    });
    $('.doctor-search .recently .part .all .notes .day p.select').on('click', function () {
        $('.doctor-search .part .notes').hide();
        $('.doctor-search .part .finish').fadeIn();
    });
    
    
    
    $('.doctor-profile-view .person .info button').on('click', function () {
        $('.doctor-profile-view .person .notes').fadeIn();
    });
    $('.doctor-profile-view .person .notes .day span').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        $('.doctor-profile-view .person .notes .day > p').addClass('select').siblings().removeClass('select');
    });
    $('.doctor-profile-view .person .notes .day p.select').on('click', function () {
        $('.doctor-profile-view .person .notes').hide();
        $('.doctor-profile-view .person .finish').fadeIn();
    });
    
    
//    $('.doctor-signup .custom .dell ul li .dropdown-toggle-one').on('click', function () {
//        $('.doctor-signup .custom .dell ul li .dropdown-menu-one');
//    });
//    
//    $('.doctor-signup .custom .dell ul li .dropdown-toggle-two').on('click', function () {
//        $('.doctor-signup .custom .dell ul li .dropdown-menu-two');
//    });
    
    
    $('.calender').datetimepicker({
        baseCls: "perfect-datetimepicker", 
        date: new Date(),
        viewMode: 'YMDHM',
        language: 'en', //I18N
        //date update event
        onDateChange: null,
        //clear button click event
        onClear: null,
        //ok button click event
        onOk: null,
        //close button click event
        onClose: null,
        //today button click event
        onToday: null
      });
    
    $('.calender-one').datetimepicker({
        date: new Date(),
        viewMode: 'YMD',
        language: 'en', //I18N
        //date update event
        onDateChange: null,
        //clear button click event
        onClear: null,
        //ok button click event
        onOk: null,
        //close button click event
        onClose: null,
        //today button click event
        onToday: null
    });
});